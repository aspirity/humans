## Crypto API

* [Swagger docs localhost](http://localhost:8080/api/swagger/)
* [Api board](https://sketchboard.me/DA39Cj6dmNyi#/)

## Configuration
* /server/src/config/index.ts
### Setup requirments
* humans.events.uri - Url for notifications (using POST with Transaction in body)
* bitcoinNetwork.host/user/password - Bitcoin node
* ethereumNetwork - Eth node

## Development

docker-compose up
