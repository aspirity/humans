import * as truffle from 'truffle-contract';
const whiteListJson = require('./build/contracts/WhiteList.json');
const PreSalePsaJson = require('./build/contracts/PreSalePSA.json');
const PreSaleSAFTJson = require('./build/contracts/PreSaleSAFT.json');
// tslint:disable-next-line
const Web3 = require('web3'); // Web3 doesn't work with ES2015 modules
import config from "./src/config";

// const provider = new Web3.providers.HttpProvider(config.get('ethereumNetwork.url'));
const PSAWhiteListAddress = '0x572334fef88e1dd7ad687f35891080b74ae35dc6';
const PSAPreSaleAddress = '';

const contractWhiteList = truffle(whiteListJson);
const contractPreSalePsa = truffle(PreSalePsaJson);

const SAFTWhiteListAddress = '0xfa2eec3d4aa578cd8d95b1b86af8ef5ebf743101';
const SAFTPreSaleAddress = '0x8742aedce6a6d515426895547119f2088268fc02';

const contractWhiteListSAFT = truffle(whiteListJson);
const contractPreSaleSAFT = truffle(PreSaleSAFTJson);

// contractWhiteList.setProvider(provider);
// contractPreSalePsa.setProvider(provider);

function fixContract(contract) {
  if (typeof contract.currentProvider.sendAsync !== "function") {
    contract.currentProvider.sendAsync = function() {
      return contract.currentProvider.send.apply(
        contract.currentProvider, arguments
      );
    };
  }
}

async function addToWhiteList(address) {
  const web3 = new Web3();
  console.log('123')
  const provider = new Web3.providers.HttpProvider(config.get('ethereumNetwork.url'));

  web3.setProvider(provider);
  console.log('567')
  await web3.eth.personal.unlockAccount(config.get("ownerAddresses.ethereum"), "");
  console.log('start')
  contractWhiteList.setProvider(provider);
  fixContract(contractWhiteList);
  const contract = await contractWhiteList.at(PSAWhiteListAddress);
  console.log('end')
  const res = await contract.addToWhiteList(address, { from: config.get("ownerAddresses.ethereum") });
  console.log('res', res);
}

const privateKey = '897f4d9279b7f5cfefaf6e871c234e3050c565caa2c8d3066c1b62991ddfa1d7';

async function addToWhiteListSAFT(address) {
  const web3 = new Web3();
  const provider = new Web3.providers.HttpProvider(config.get('ethereumNetwork.url'));

  web3.setProvider(provider);
  console.log('importing');
  web3.eth.personal.importRawKey(privateKey, ''); // 'password' ?
  console.log('imported');
  console.log('567')
  await web3.eth.personal.unlockAccount(config.get("ownerAddresses.ethereum"), "");
  console.log('start')
  contractWhiteListSAFT.setProvider(provider);
  fixContract(contractWhiteListSAFT);
  const contract = await contractWhiteListSAFT.at(SAFTWhiteListAddress);
  console.log('end')
  const res = await contract.addToWhiteList(address, { from: config.get("ownerAddresses.ethereum") });
  console.log('res', res);
}

async function addTokens(address) {
  const web3 = new Web3();
  const provider = new Web3.providers.HttpProvider(config.get('ethereumNetwork.url'));

  web3.setProvider(provider);
  console.log('importing');
  web3.eth.personal.importRawKey(privateKey, ''); // 'password' ?
  console.log('imported');
  console.log('567')
  await web3.eth.personal.unlockAccount(config.get("ownerAddresses.ethereum"), "");
  console.log('start')
  contractPreSaleSAFT.setProvider(provider);
  fixContract(contractPreSaleSAFT);
  const contract = await contractPreSaleSAFT.at(SAFTPreSaleAddress);
  console.log('end', new Date().toLocaleTimeString())
  const res = await contract.manualBuy(address, 2*Math.pow(10, 18), { from: config.get("ownerAddresses.ethereum"), gas: 100000 });
  console.log('res', new Date().toLocaleTimeString(), res);
}

// addToWhiteList('0x5D7e0024C5A28427ad9FEAeF5b3BD350d9D75c72').then(() => {}).catch(e => console.log(e));

addToWhiteListSAFT('0x5D7e0024C5A28427ad9FEAeF5b3BD350d9D75c72').then(() => console.log('test finished')).catch(e => console.log(e));
// addTokens('0x5D7e0024C5A28427ad9FEAeF5b3BD350d9D75c72').then(() => console.log('test finished')).catch(e => console.log(e));

setTimeout(() => {}, 9999999);
