var HumansToken = artifacts.require("./HumansToken.sol");
var PreSalePSA = artifacts.require("./PreSalePSA.sol");
var PreSaleSAFT = artifacts.require("./PreSaleSAFT.sol");
var WhiteListPSA = artifacts.require("./WhiteList.sol");
var WhiteListSAFT = artifacts.require("./WhiteList.sol");
// var CrowdSale = artifacts.require("./CrowdSale.sol");
// var WhiteListCrowdSale = artifacts.require("./WhiteList.sol");
// var TokenTimelock = artifacts.require("./TokenTimelock.sol");

const startTime = Math.round((new Date().getTime())/1000); // Now
const endTime = Math.round((new Date().getTime() + 604800000)/1000); // Now + 7 days
const wallet = '0x627306090abaB3A6e1400e9345bC60c78a8BEf57';
const oracle = '0x627306090abaB3A6e1400e9345bC60c78a8BEf57';

module.exports = function(deployer) {
  deployer.deploy(HumansToken).then(function() {
    return deployer.deploy(WhiteListPSA);
  }).then(function() {
    return deployer.deploy(WhiteListSAFT);
  }).then(function() {
    return deployer.deploy(PreSalePSA, startTime, endTime, wallet, oracle,
      HumansToken.address, WhiteListPSA.address);
  }).then(function() {
    return deployer.deploy(PreSaleSAFT, startTime, endTime, wallet, oracle,
      HumansToken.address, WhiteListSAFT.address);
  }).then(function() {
    return PreSalePSA.deployed();
  }).then(function(InstancePSA) {
    return InstancePSA.setPreSaleSAFT(PreSaleSAFT.address);
  }).then(function() {
    return PreSaleSAFT.deployed();
  }).then(function(InstanceSAFT) {
    return InstanceSAFT.setPreSalePSA(PreSalePSA.address);
  })
};
