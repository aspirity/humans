import * as truffle from 'truffle-contract';
import * as Bitcoin from 'bitcoin-core';
import config from '../config';
import Logger from '../Logger';
const whiteListJson = require('../../build/contracts/WhiteList.json');
const crowdSaleJson = require('../../build/contracts/CrowdSale.json');
const preSaleSAFTJson = require('../../build/contracts/PreSaleSAFT.json');

const logger = new Logger();

import CryptoRatesModel from '../models/CryptoRatesModel';
import UserModel, { WhiteListType } from '../models/UserModel';
import { TransactionStatus, TransactionType } from '../models/TransactionModel';
import EventsService from './EventsService';

const Web3 = require('web3'); // Web3 doesn't work with ES2015 modules

export class CryptoService {
  private provider;
  private web3;
  private whiteListCrowdSale;
  private whiteListSAFT;
  private crowdSale;
  private preSaleSAFT;

  public async initialize() {
    this.tokenReceived = this.tokenReceived.bind(this);

    const url = config.get('ethereumNetwork.url');
    const privateKey = config.get('ownerAddresses.ethPrivate');

    this.web3 = new Web3();
    this.provider = new Web3.providers.HttpProvider(url);
    this.web3.setProvider(this.provider);
    this.web3.eth.personal.importRawKey(privateKey, '');

    await this.web3.eth.personal.unlockAccount(
      config.get('ownerAddresses.ethereum'),
      config.get('ownerAddresses.ethPassword'),
    );

    const whiteListContract = truffle(whiteListJson);
    whiteListContract.setProvider(this.provider);

    const crowdSaleContract = truffle(crowdSaleJson);
    crowdSaleContract.setProvider(this.provider);

    const preSaleSAFTcontract = truffle(preSaleSAFTJson);
    preSaleSAFTcontract.setProvider(this.provider);

    this.fixContract(whiteListContract);
    this.fixContract(crowdSaleContract);
    this.fixContract(preSaleSAFTcontract);

    this.whiteListCrowdSale = await whiteListContract.at(config.get('contracts.whiteList.CrowdSale'));
    this.whiteListSAFT = await whiteListContract.at(config.get('contracts.whiteList.SAFT'));

    this.crowdSale = await crowdSaleContract.at(config.get('contracts.crowdSale'));

    this.preSaleSAFT = await preSaleSAFTcontract.at(config.get('contracts.preSaleSAFT'));

    console.log('Contracts set up');

    this.crowdSale.TokenPurchase().watch(this.onTokenReceived.bind(this));
    this.preSaleSAFT.TokenPurchase().watch(this.onTokenReceived.bind(this));
    // this.crowdSale.TokenRefund().watch(this.onTokenReceived.bind(this));
  }

  private getNumber(bigNumber) {
    if (!bigNumber) return null;
    const number = bigNumber.toNumber ? bigNumber.toNumber() : bigNumber;
    return number * Math.pow(10, -18);
  }

  private async tokenReceived(err, result) {

    if (!err && !result) {
      logger.error('onTokenReceived - args are empty');
      throw 'Event triggered without params';
    }
    const txHash = result ? result.transactionHash : (err && err.transactionHash);
    let weiReceived = null;
    try {
      // For manual eth transfer we need to get transaction info and extract amount of eth transferred
      const ethTransaction = await this.web3.eth.getTransactionFromBlock(result.blockNumber, result.transactionIndex);
      weiReceived = ethTransaction ? this.getNumber(ethTransaction.value) : 0;
      // For SAFT contract - there's no info about amount of eth transferred in transaction,
      // but SAFT.manulaBy has "weiReceived" field, calculated on contract level
      if (!weiReceived && result.args.weiReceived) {
        weiReceived = this.getNumber(result.args.weiReceived);
      }
    } catch (e) {
      logger.error(`Failed to fetch transaction data: ${txHash}`, e);
    }
    if (err && !result) {
      logger.error('Transaction failed', err);
      const transaction = {
        status: TransactionStatus.declined,
        transactionHash: txHash,
        meta: {
          result, // always null
          error: { error: err, message: err.message },
        },
      } as any;
      return await EventsService.createTransaction(transaction);
    }
    const args = result.args || {};
    logger.info(`tokens received, tx: ${txHash}`);
    const ethWallet = args.beneficiary;
    const tokens = this.getNumber(args.numberOfTokens);

    const user = await UserModel.findOne({ ethAddress: { $regex: ethWallet.toString(), $options: 'i' } }).lean();
    const transaction = {
      status: err ? TransactionStatus.declined : TransactionStatus.confirmed,
      amountReceived: tokens,
      transactionHash: txHash,
      meta: {
        confirmed: result,
      },
    } as any;
    if (user) {
      transaction.user = user;
    }
    // if weiReceived is null or zero (calculated above)
    // then this transaction is manualBy and amountPaid already known
    if (weiReceived && this.getNumber(weiReceived) > 0) {
      transaction.amountPaid = weiReceived;
      transaction.currencyType = 'ETH';
    }
    return await EventsService.createTransaction(transaction);
  }

  private async onTokenReceived(err, result) {
    const env = config.get('env');
    if (env === 'production')
      return this.tokenReceived(err, result);
    const that = this;
    // await 60sec in case of getting event earlier than node response
    setTimeout(() => that.tokenReceived(err, result), 60 * 1000);
  }

  private async unlockAccount() {
    await this.web3.eth.personal.unlockAccount(
      config.get('ownerAddresses.ethereum'),
      config.get('ownerAddresses.ethPassword'),
    );
  }

  private fixContract(contract) {
    if (typeof contract.currentProvider.sendAsync !== 'function') {
      contract.currentProvider.sendAsync = function () {
        return contract.currentProvider.send.apply(
          contract.currentProvider, arguments,
        );
      };
    }
  }

  private getContractTransactionParams() {
    return {
      from: config.get('ownerAddresses.ethereum'),
      gas: config.get('contracts.gasLimit'),
    };
  }

  public validateEthereumAddress(address) {
    const web3 = new Web3();

    web3.setProvider(new Web3.providers.HttpProvider(config.get('ethereumNetwork.url')));

    return web3.utils.isAddress(address);
  }

  public async addToWhiteList(address, type) {
    await this.unlockAccount();
    const contract = type === WhiteListType.saft ? this.whiteListSAFT : this.whiteListCrowdSale;
    try {
      const res = await contract.addToWhiteList(address, this.getContractTransactionParams());
      return res;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  public async getBalance(address, type) {
    const contract = type === WhiteListType.saft ? this.preSaleSAFT : this.crowdSale;
    try {
      const result = await contract.tokenBalances(address);

      return this.getNumber(result);
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  public async transferTokens(address, type, amount) {
    console.log(address, type, amount);
    await this.unlockAccount();
    const contract = type === WhiteListType.saft ? this.preSaleSAFT : this.crowdSale;
    try {
      const result = await contract.manualBuy(address, amount * Math.pow(10, 18), this.getContractTransactionParams());
      return result;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  public async refundTokens(transactionId) {
    await this.unlockAccount();
    const contract = this.crowdSale;
    try {
      const transaction = await contract.manualRefund(transactionId, this.getContractTransactionParams());
      return transaction;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  async getExchangeRates() {
    const cryptoRates = await CryptoRatesModel.findOne({}, {}, { sort: { createdAt: -1 } });

    return {
      usdToToken: 1,
      ethToToken: Number(cryptoRates.ethereumPrice).toFixed(),
      btcToToken: Number(cryptoRates.bitcoinPrice).toFixed(),
    };
  }

  async changeRate(type: WhiteListType, newRate: number) {
    const contract = type === WhiteListType.saft ? this.preSaleSAFT : this.crowdSale;
    try {
      const result = await contract.changeRate(newRate, this.getContractTransactionParams());
      return result;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  public getTokenPrice(tsm) {
    const contract = tsm === WhiteListType.saft ? this.preSaleSAFT : this.crowdSale;
    return 1; // todo:
  }

  // region bitcoin
  public getBitcoin() {
    return new Bitcoin({
      port: config.get('bitcoinNetwork.port'),
      host: config.get('bitcoinNetwork.host'),
      username: config.get('bitcoinNetwork.username'),
      password: config.get('bitcoinNetwork.password'),
      ssl: {
        enabled: false,
        strict: false,
      },
    });
  }

  public async getTransactionInfo(txId) {
    const bitcoin = this.getBitcoin();
    return bitcoin.getTransaction(txId);
  }

  public async createBitcoinAddress(id) {
    const bitcoin = this.getBitcoin();
    return await bitcoin.getNewAddress(id);
  }

  public async getBitcoinBalance(id) {
    const bitcoin = this.getBitcoin();

    return bitcoin.getBalance(id);
  }

  public async getBitcoinTransactions(btcAddress) {
    const bitcoin = this.getBitcoin();

    // minimum confirmation is 1
    return await bitcoin.listUnspent(1, 99999999, [btcAddress]);
  }

  public async unlockAllTransactions() {
    const bitcoin = this.getBitcoin();
    const lockedTransactions = await bitcoin.listLockUnspent();
    await  bitcoin.lockUnspent(true, lockedTransactions);
  }

  public async lockTransaction(txid, vout) {
    logger.info(`lock transaction ${txid}`);
    const bitcoin = this.getBitcoin();
    await bitcoin.lockUnspent(false, [{ txid, vout }]);
  }

  public async unlockTransaction(txid, vout) {
    logger.info(`unlock transaction ${txid}`);
    const bitcoin = this.getBitcoin();
    await bitcoin.lockUnspent(true, [{ txid, vout }]);
  }

  public async transferAllBitcoins(amounts) {
    const bitcoin = this.getBitcoin();
    const balance = await bitcoin.getBalance();

    const transfer = async (amount, where) => {
      logger.info(`transfer ${amount} bitcoins to ${where}`);
      try {
        await bitcoin.sendToAddress(where, amount.toFixed(8));
        const newBalance = await bitcoin.getBalance();
        logger.info(`new balance: ${newBalance}`);
      } catch (err) {
        console.log(err.message);
        if (err.code === -4) {
          const fee = parseFloat(err.message.split(' ').pop());

          try {
            // bitcoin-node cannot represent values smaller than 10^-8
            const newAmount = (amount - fee).toFixed(8);
            await bitcoin.sendToAddress(where, newAmount);
          } catch (err) {
            logger.error(err);
          }
        } else {
          logger.error(err);
        }
      }
    };

    if (balance > 0) {
      logger.info(`balance: ${balance}`);
      if (amounts.crowdSaleAmountToTransfer > 0) {
        await transfer(amounts.crowdSaleAmountToTransfer, config.get('ownerAddresses.bitcoin.CrowdSale'));
      }else {
        logger.info('nothing to transfer for CrowdSale');
      }
      const residualBalance = await bitcoin.getBalance();

      if (amounts.saftAmountToTransfer > 0) {
        // if we don't have enought balance to send it after first transaction
        if (residualBalance < amounts.saftAmount) {
          await transfer(residualBalance - amounts.nonTransferableSaft, config.get('ownerAddresses.bitcoin.SAFT'));
        }else {
          await transfer(amounts.saftAmountToTransfer, config.get('ownerAddresses.bitcoin.SAFT'));
        }
      }else {
        logger.info('nothing to transfer for SAFT');
      }
    }else {
      logger.info('nothing to transfer');
    }
  }
}

const cryptoService = new CryptoService();

export default cryptoService;
