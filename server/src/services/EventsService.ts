import axios from 'axios';
import config from '../config';
import Logger from '../Logger';
import TransactionModel, { Transaction, TransactionStatus } from '../models/TransactionModel';

const logger = new Logger();

class EventsService {
  async notifyExternal(transaction) {
    try {
      await axios.post(config.get('humans.events.uri'), transaction);
    } catch (e) {
      logger.error('External system notification failed', e);
    }
  }

  async createTransaction(transaction) {
    let dbTransaction = {};
    const pendingTransaction = transaction.transactionHash &&
      await TransactionModel.findOne({ transactionHash: transaction.transactionHash }).lean();
    // event could happen before actual tx result (rarely, but possible)
    const status = (pendingTransaction && pendingTransaction.status === TransactionStatus.confirmed ||
      transaction.status === TransactionStatus.confirmed) ? TransactionStatus.confirmed : transaction.status;
    const updTx = {
      ...pendingTransaction,
      ...transaction,
      status,
      meta: {
        ...transaction.meta,
        ...(pendingTransaction && pendingTransaction.meta),
      },
    };
    if (pendingTransaction) {
      dbTransaction = await TransactionModel.findOneAndUpdate({ _id: pendingTransaction._id }, updTx, {new: true});
    } else {
      dbTransaction = await TransactionModel.create(updTx);
    }
    await this.notifyExternal(dbTransaction);
    return dbTransaction;
  }
}

export default new EventsService();
