import * as convict from 'convict';
import * as path from 'path';

const config = convict({
  env: {
    doc: 'Application environment',
    format: ['production', 'dev', 'qa', 'demo'],
    default: 'dev',
    env: 'NODE_ENV',
  },
  http: {
    url: {
      doc: 'Base server url',
      format: String,
      default: 'http://localhost:8080',
      env: 'APP_URL',
    },
    port: {
      doc: 'Base server port',
      format: 'port',
      default: 8080,
      env: 'APP_PORT',
    },
  },
  version: {
    doc: 'Current version',
    format: String,
    default: 'v1',
    env: 'APP_VERSION',
  },
  db: {
    uri: {
      doc: 'Mongodb connection URI',
      format: String,
      default: 'mongodb://admin:phd4ex16t34i7q230@humans_mongodb:27017/Humans?authSource=admin',
      env: 'MONGO_URI',
    },
  },
  humans: {
    events: {
      uri: {
        doc: 'Humans API related to accept transactions',
        format: String,
        default: 'humans_server:8080/api/v1/crypto/transaction/accept',
      },
    },
  },
  cryptoRates: {
    bitcoinUrl: {
      doc: 'External api url for fetching bitcoin price',
      format: String,
      default: 'https://api.coinmarketcap.com/v1/ticker/bitcoin/',
    },
    ethereumUrl: {
      doc: 'External api url for fetching ethereum price',
      format: String,
      default: 'https://api.coinmarketcap.com/v1/ticker/ethereum/',
    },
  },
  contracts: {
    token: {
      doc: 'Humans gen token contract address',
      format: String,
      default: '0x3ba2c9052a35c97e27e95a23b1032415deda3121',
    },
    whiteList: {
      CrowdSale: {
        doc: 'CrowdSale whiteList contract address',
        format: String,
        default: '0xee01405f3495b53d5520242f3c5a5af3f8d73953',
      },
      SAFT: {
        doc: 'SAFT whiteList contract address',
        format: String,
        default: '0x6a102d01f81ea8018567eb236ef5f7490cc47962',
      },
    },
    crowdSale: {
      doc: 'PreSale CrowdSale contract address',
      format: String,
      default: '0x6e06ab9be49ca6c00413e62da187b4ab7425e566',
    },
    preSaleSAFT: {
      doc: 'PreSale SAFT contract address',
      format: String,
      default: '0xd206f1c8423c4728b4fdf56acd9aa1a9a95e1c8e',
    },
    gasLimit: {
      doc: 'Limit of gas for each transaction',
      format: String,
      default: '100000',
    },
  },
  ownerAddresses: {
    ethereum: {
      doc: 'Owners\'s ethereum address',
      format: String,
      default: '0xdcB8e4AAfb6dF2C5BBc7EBfe4BDcb6B4668F69F2',
    },
    ethPassword: {
      doc: 'Owners password',
      format: String,
      default: '',
    },
    ethPrivate: {
      doc: 'Owners private key, used for owner transactions',
      format: String,
      default: '1c3755154f1636e758597a88d9a8b7942106321b9f4e6dc9000e01137424011e',
    },
    bitcoin: {
      CrowdSale: {
        doc: 'CrowdSale owners\'s bitcoin address',
        format: String,
        default: '2MzhQNRqzjxGLNVMGji1tGz6U5FSRw7AQ88',
      },
      SAFT: {
        doc: 'SAFT owners\'s bitcoin address',
        format: String,
        default: '2MzhQNRqzjxGLNVMGji1tGz6U5FSRw7AQ88',
      },
    },
  },
  bitcoinNetwork: {
    host: {
      doc: 'Bitcoin network\'s host',
      format: String,
      default: 'docker.aspirity.com',
    },
    port: {
      doc: 'Bitcoin network\'s port',
      format: Number,
      default: 12001,
    },
    username: {
      doc: 'Bitcoin network\'s username',
      format: String,
      default: 'AspirityBitcoin',
    },
    password: {
      doc: 'Bitcoin network\'s password',
      format: String,
      default: 'UPimBaf2oVjYdkdJbEUv6CjZ',
    },
  },
  ethereumNetwork: {
    url: {
      doc: 'Ethereum network\'s url',
      format: String,
      default: 'http://188.42.133.236:8545',
    },
  },
});

const env = config.get('env');
const envFile = path.resolve(__dirname, 'env', `${env}.json`);

config.loadFile(envFile);
config.validate();

export default config;
