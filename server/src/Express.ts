import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as swaggerUi from 'swagger-ui-express';
import * as YAML from 'yamljs';
import RequestsLogger from './RequestsLogger';
import customValidators from './middlewares/customValidators';
import MainController from './controllers/MainController';

import './tasks'; // start workers

const swaggerDocument = YAML.load('./src/swagger.yml');
const requestsLogger = new RequestsLogger();

class Express {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.initPreRoutesMiddlewares();
    this.initRoutes();
    this.initPostRoutesMiddlewares();
  }

  private initPreRoutesMiddlewares(): void {
    this.app.use(bodyParser.json());
    this.app.use(requestsLogger.all());
    this.app.use(customValidators());
  }

  private initRoutes(): void {
    const mainController = new MainController();
    mainController.init();
    this.app.use('/api', mainController.getRouter());

    this.app.use('/api/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }

  private initPostRoutesMiddlewares() {
    this.app.use(requestsLogger.errors());
  }
}

export default Express;
