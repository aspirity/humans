import * as cron from 'cron';
import axios from 'axios';
import CryptoRatesModel from '../models/CryptoRatesModel';
import config from '../config';
import Logger from '../Logger';

const logger = new Logger();

const fetchCryptoRates = new cron.CronJob('0 */10 * * * *', async () => {
  try {
    const bitcoinUrl = config.get('cryptoRates').bitcoinUrl;
    const ethereumUrl = config.get('cryptoRates').ethereumUrl;
    const bitcoinResponse = await axios.get(bitcoinUrl);
    const ethereumResponse = await axios.get(ethereumUrl);

    const cryptoRates = new CryptoRatesModel();

    cryptoRates.bitcoinPrice = bitcoinResponse.data[0].price_usd;
    cryptoRates.ethereumPrice = ethereumResponse.data[0].price_usd;

    await cryptoRates.save();

    const count = await CryptoRatesModel.count({});

    // Store last 50 documents
    if (count > 50) {
      await CryptoRatesModel.findOneAndRemove({}, { sort: { createdAt: 1 } });
    }
  } catch (error) {
    logger.error(error);
  }
},                                        null, true);

export default fetchCryptoRates;
