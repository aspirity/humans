import * as cron from 'cron';
import Logger from '../Logger';
import CryptoService from '../services/CryptoService';
import UserModel, { WhiteListType } from '../models/UserModel';
import CryptoRatesModel from '../models/CryptoRatesModel';
import TransactionModel, { TransactionStatus, TransactionType } from '../models/TransactionModel';

import EventsService from '../services/EventsService';

const logger = new Logger();

export async function moveBitcoins() {
  try {
    const users = await UserModel.find({}).lean();
    const exchangeRate = await CryptoRatesModel.findOne().sort({ createdAt: -1 }).lean();
    if (!exchangeRate || !exchangeRate.bitcoinPrice) {
      console.log('Can`t transfer bitcoins, exchange rates not fetched yet');
      return;
    }
    const btcPrice = Number(exchangeRate.bitcoinPrice);
    const transactions = await TransactionModel.find({
      $or: [
        { status: TransactionStatus.awaiting },
        { status: TransactionStatus.confirmed },
        { status: TransactionStatus.unprocessable },
      ],
    }).lean();
    let saftAmount = Number(0);
    let crowdSaleAmount = Number(0);
    let nonTransferableSaft = Number(0);
    let nonTransferableCrowd = Number(0);
    try {
      // we can't lock already locked
      await CryptoService.unlockAllTransactions();
    } catch (e) {
      logger.error(e);
      throw e;
    }
    let amountNotToTransfer = 0;
    const minAmount = 50;
    const transferTokens = async () => {
      await Promise.all(users.map(async (user) => {
        if (!user.humansId) return;

        const bitcoinTransactions = await CryptoService.getBitcoinTransactions(user.btcAddress);

        await Promise.all(bitcoinTransactions.map(async (bt) => {
          // if we find bitcoinTransactionId in successful transactions we don't need to transfer tokens
          if (transactions.filter(t => t.meta.bitcoinTransactionId === bt.txid).length <= 0) {
            try {
              // lock transaction in order to nobody can transfer its bitcoins to another address
              // till tokens are transferred
              await CryptoService.lockTransaction(bt.txid, bt.vout);
              const amount = bt.amount * btcPrice;
              if (amount < minAmount) {
                const transaction = {
                  user,
                  exchangeRate,
                  status: TransactionStatus.unprocessable,
                  amountPaid: bt.amount,
                  amountReceived: null,
                  currencyType: 'BTC',
                  type: TransactionType.buy,
                  transactionHash: null,
                  bitcoinTransactionId: bt.txid,
                  meta: {
                    declined: { message: `${amount} is lower than allowed, expected above ${minAmount}` },
                    bitcoinTransactionId: bt.txid,
                    bitcoinTransaction: bt,
                  },
                };
                try {
                  await EventsService.createTransaction(transaction);
                } catch (e) { logger.error(e); }
                amountNotToTransfer += bt.amount;
                if (user.whiteListType === WhiteListType.crowd) {
                  nonTransferableCrowd += bt.amount;
                }
                if (user.whiteListType === WhiteListType.saft) {
                  nonTransferableSaft += bt.amount;
                }
                return;
              }

              const tx = await CryptoService.transferTokens(user.ethAddress, user.whiteListType, amount);
              if (user.whiteListType === WhiteListType.crowd) {
                crowdSaleAmount += bt.amount;
              }
              if (user.whiteListType === WhiteListType.saft) {
                saftAmount += bt.amount;
              }
              await CryptoService.unlockTransaction(bt.txid, bt.vout);
              const transaction = {
                user,
                exchangeRate,
                status: TransactionStatus.awaiting,
                amountPaid: bt.amount,
                currencyType: 'BTC',
                type: TransactionType.buy,
                transactionHash: tx.tx,
                meta: {
                  awaiting: tx,
                  bitcoinTransactionId: bt.txid,
                },
              };
              try {
                await EventsService.createTransaction(transaction);
              }catch (e) { logger.error(e); }
            } catch (e) {
              logger.error(e.message);
              // we can't transfer locked amount
              amountNotToTransfer += bt.amount;
              if (user.whiteListType === WhiteListType.crowd) {
                nonTransferableCrowd += bt.amount;
              }
              if (user.whiteListType === WhiteListType.saft) {
                nonTransferableSaft += bt.amount;
              }
              // in case of "-8" (locked input) - we are not going to do anything, because prev operation
              // still being processed
              if (e.code === -8) {
                logger.error('Transaction in progress, skipping: ', bt.txid);
                return;
              }
              const transaction = {
                user,
                exchangeRate,
                status: TransactionStatus.declined,
                amountPaid: bt.amount,
                amountReceived: null,
                currencyType: 'BTC',
                type: TransactionType.buy,
                transactionHash: null,
                bitcoinTransactionId: bt.txid,
                meta: {
                  declined: { error: e, message: e.message },
                  bitcoinTransactionId: bt.txid,
                },
              };
              try {
                await EventsService.createTransaction(transaction);
              } catch (e) { logger.error(e); }
            }
          }
        }));
      }));
    };

    try {
      await transferTokens();
    }catch (error) {
      logger.error(error);
    }
    const amounts = {
      nonTransferableCrowd,
      nonTransferableSaft,
      saftAmount,
      crowdSaleAmount,
      saftAmountToTransfer: saftAmount - nonTransferableSaft,
      crowdSaleAmountToTransfer: crowdSaleAmount - nonTransferableCrowd,
    }
    await CryptoService.transferAllBitcoins(amounts);

  } catch (error) {
    logger.error(error);
  }
}

const buyTokensForBitcoins = new cron.CronJob('0 */10 * * * *', async () => {
  await moveBitcoins();
},                                            null, true);

export default buyTokensForBitcoins;
