import { Request, Response, NextFunction } from 'express';
import * as mongoose from 'mongoose';
import Logger from '../../Logger';
import BaseController from '../BaseController';
import UserModel, { User, WhiteListType } from '../../models/UserModel';
import CryptoService from '../../services/CryptoService';
import TransactionModel, { TransactionStatus, TransactionType } from '../../models/TransactionModel';
import CryptoRatesModel from '../../models/CryptoRatesModel';
import EventsService from '../../services/EventsService';

const logger = new Logger();

class ExampleController extends BaseController {
  public init(): void {
    this.router.post('/whiteList', this.addToWhiteList);
    this.router.get('/:id/balance', this.getUserBalance);
    this.router.post('/:id/add', this.addTokens);
    this.router.post('/:id/refund', this.refundTokens);

    this.router.get('/whitelisted', this.getWhiteListed);
    this.router.get('/transaction/list', this.getTransactions);

    this.router.get('/rates', this.getCryptoRates);
    this.router.get('/calculate', this.calculateTokens);

    this.router.post('/changeRate', this.changeRate);

    // for tests purpose
    this.router.post('/transaction/accept', this.acceptTransaction);

    this.router.post('/bitcoin-hook', this.postHook);

    CryptoService.initialize()
      .then(() => console.log('cryptoService up'))
      .catch((e) => {
        console.log('cryptoService init failed', e);
      });
  }

  public async postHook(req: Request, res: Response, next:NextFunction): Promise<Response> {
    logger.info('CryptoController postHook route entered');
    try {
      const txId = req.body.txid;
      const txInfo = await CryptoService.getTransactionInfo(txId);
      if (txInfo.confirmations < 1) {
        return res.sendStatus(200);
      }
      if (txInfo) {
        const exchangeRate = await CryptoRatesModel.findOne().sort({ createdAt : -1 }).lean();
        const btcPrice = Number(exchangeRate.bitcoinPrice);
        const address = txInfo.details[0].address;
        const user = await UserModel.findOne({ btcAddress: address }).lean();
        if (user) {
          const amount = txInfo.amount * btcPrice;
          try {
            const tx = await CryptoService.transferTokens(user.ethAddress, user.whiteListType, amount);
            const transaction = {
              user,
              exchangeRate,
              status: TransactionStatus.awaiting,
              amountPaid: txInfo.amount,
              currencyType: 'BTC',
              type: TransactionType.buy,
              transactionHash: tx.tx,
              meta: {
                awaiting: tx,
                bitcoinTransactionid:txId,
              },
            };
            await EventsService.createTransaction(transaction); //
          } catch (e) {
            const transaction = {
              user,
              exchangeRate,
              status: TransactionStatus.declined,
              amountPaid: txInfo.amount,
              amountReceived: null,
              currencyType: 'BTC',
              type: TransactionType.buy,
              transactionHash: null,
              meta: {
                declined: { error: e, message: e.message },
                bitcoinTransactionid:txId,
              },
            };
            await EventsService.createTransaction(transaction);
          }

        }
        return res.sendStatus(200);

      }
    } catch (e) {
      return res.sendStatus(500);
    }
  }

  public async addToWhiteList(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController addToWhiteList route entered');

    const user = req.body;
    const { ethAddress, whiteListType, humansId } = user;

    let btcAddress = '';

    try {
      btcAddress = await CryptoService.createBitcoinAddress(humansId);

    } catch (e) {
      logger.error(e);
      return res.status(500).send('Failed to create btc address');
    }

    try {
      await CryptoService.addToWhiteList(ethAddress, whiteListType);
    } catch (e) {
      logger.error(e);
      return res.status(500).send('Failed to add user to WL');
    }

    const userDoc = {
      ethAddress,
      btcAddress,
      whiteListType,
      humansId,
    };

    const createdUser = await UserModel.findOneAndUpdate({ _id:mongoose.Types.ObjectId() }, userDoc, {
      new: true,
      upsert: true,
      runValidators: true,
      setDefaultsOnInsert: true,
    });

    return res.json(createdUser);
  }

  public async getUserBalance(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController getUserBalance route entered');

    const { id } = req.params;
    const user = await UserModel.findById(id).lean();
    if (!user) return res.status(404).send('Not found');

    const balance = await CryptoService.getBalance(user.ethAddress, user.whiteListType);
    return res.json({ balance, _id: id });
  }

  public async addTokens(req: Request, res: Response, next: NextFunction): Promise<Response|void> {
    logger.info('CryptoController addTokens route entered');

    const { id } = req.params;
    const { amount, channel } = req.body;
    let user = null;
    try {
      user = await UserModel.findById(id).lean();
    } catch (e) {
      return res.status(404).send('Not found');
    }
    if (!user) return res.status(404).send('Not found');

    try {
      const tx = await CryptoService.transferTokens(user.ethAddress, user.whiteListType, amount);
      const transaction = {
        user,
        channel,
        status: TransactionStatus.awaiting,
        amountPaid: amount,
        currencyType: 'USD', // same, remove required
        type: TransactionType.buy,
        transactionHash: tx.tx, // same here,
        meta: {
          awaiting: tx,
        },
      };
      await EventsService.createTransaction(transaction);
      return res.status(200).send('Success');
    } catch (e) {
      return next(e);
    }
  }

  public async refundTokens(req: Request, res: Response, next: NextFunction): Promise<Response|void> {
    logger.info('CryptoController refundTokens route entered');

    const { id } = req.params;
    const { transactionId } = req.body;
    const user = await UserModel.findById(id).lean();
    if (!user) return res.status(404).send('Not found');

    try {
      const tx = await CryptoService.refundTokens(transactionId);
      const transaction = {
        user,
        status: TransactionStatus.awaiting,
        amountReceived: null,
        currencyType: 'USD',
        type: TransactionType.refund,
        transactionHash: tx.tx,
        meta: {
          awaiting: tx,
        },
      };
      await EventsService.createTransaction(transaction);
      return res.status(200).send('Success');
    } catch (e) {
      return next(e);
    }
  }

  public async getWhiteListed(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController getWhiteListed route entered');
    const whitelisted = await UserModel.find({ whiteListType: { $ne: WhiteListType.none } });
    return res.json(whitelisted);
  }

  public async getTransactions(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController getTransactions route entered');
    const transactions = await TransactionModel.find();
    return res.json(transactions);
  }

  public async getCryptoRates(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController getCryptoRates route entered');
    const cryptoRates = await CryptoRatesModel.findOne({}, {}, { sort: { createdAt: -1 } });

    if (!cryptoRates) {
      return res.status(204).send('No crypto rates found');
    }

    return res.json(cryptoRates);
  }

  public async calculateTokens(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController calculateTokens route entered');
    const { amount, type } = req.query;
    if (!amount) return res.status(400).send('Amount required');
    if (!type) return res.status(400).send('Type required');
    const exchangeRates = await CryptoService.getExchangeRates();

    let rate = '';
    switch (type.toUpperCase()) {
      case 'BTC':
        rate = exchangeRates.btcToToken;
        break;
      case 'ETH':
        rate = exchangeRates.ethToToken;
        break;
      default:
        return res.status(400).send('Unknown type');
    }

    return res.json({ ...exchangeRates, result: Number(rate) * amount });
    // return res.json(exchangeRates);
  }

  public async changeRate(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController changeRate route entered');
    const { newRate, type } = req.body;
    try {
      const changed = await CryptoService.changeRate(type, newRate);
      return res.json(changed);
    } catch (e) {
      return res.status(500).send(e);
    }
  }

  public async acceptTransaction(req: Request, res: Response, next: NextFunction): Promise<Response> {
    logger.info('CryptoController acceptTransaction route entered, txId: ', req.body && req.body._id);
    const { body } = req;

    return res.json(body);
    // return res.json(exchangeRates);
  }
}

export default ExampleController;
