import { prop, Typegoose, ModelType, InstanceType } from 'typegoose';
import * as mongoose from 'mongoose';
import { User } from './UserModel';
import { CryptoRates } from './CryptoRatesModel';

export enum TransactionStatus {
  declined = 'Declined',
  confirmed = 'Confirmed',
  awaiting = 'Awaiting',
  unprocessable = 'Unprocessable',
  refunded = 'Refunded',
}

export enum TransactionType {
  buy = 'Buy',
  refund = 'Refund',
}

export class Transaction extends Typegoose {
  public _id: mongoose.Types.ObjectId;

  @prop({ ref: User, required: false }) // user would be null for unknow users transactions (still logging them)
  user: User;

  @prop({ enum: TransactionStatus, default: TransactionStatus.awaiting })
  status: TransactionStatus;

  @prop({ required: false, default: 0 })
  amountPaid: number;

  @prop({ default: 'USD' })
  currencyType: string;

  @prop({ required: false, default: 0 })
  amountReceived: number;

  @prop({ enum: TransactionType, default: TransactionType.buy })
  type: TransactionType;

  @prop({ default: null })
  transactionHash: string;

  @prop({ default: null })
  exchangeRate?: CryptoRates;

  @prop({ default: 'Crypto' })
  channel: string;

  @prop({ default: null, required: false })
  meta: object;
}

const TransactionModel = new Transaction().getModelForClass(Transaction, { schemaOptions: { timestamps: true } });

export default TransactionModel;
