import { Typegoose, prop } from 'typegoose';
import * as mongoose from 'mongoose';

export enum WhiteListType {
  crowd = 'CrowdSale',
  saft = 'SAFT',
  ico = 'ICO',
  none = 'NONE',
}

export class User extends Typegoose {
  public _id: mongoose.Types.ObjectId;

  @prop({ required: true })
  public humansId: string;

  @prop({ required: true })
  public ethAddress: string;

  @prop({ required: true })
  public btcAddress: string;

  @prop({ enum: WhiteListType, required: true, default: WhiteListType.none })
  public whiteListType: WhiteListType;
}

const UserModel = new User().getModelForClass(User);

export default UserModel;
