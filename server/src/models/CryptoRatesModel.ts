import { prop, Typegoose, ModelType, InstanceType } from 'typegoose';

export class CryptoRates extends Typegoose {
  @prop({ required: true })
  public bitcoinPrice: string;

  @prop({ required: true })
  public ethereumPrice: string;
}

const CryptoRatesModel = new CryptoRates().getModelForClass(CryptoRates, { schemaOptions: { timestamps: true } });

export default CryptoRatesModel;
