/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./ownership/Ownable.sol";


contract WhiteList is Ownable {

    event WhiteListedAddressAdded(address addr);
    event WhiteListedAddressRemoved(address addr);

    mapping(address => bool) public accreditedInvestor;
    mapping(address => bool) public admins;

    modifier onlyAdminOrOwner() {
        require(admins[msg.sender] || isOwner());
        _;
    }

    function _addToWhiteList(address _investor)
        internal
    {
        require(_investor != address(0));
        accreditedInvestor[_investor] = true;
        emit WhiteListedAddressAdded(_investor);
    }

    function _removeFromWhiteList(address _investor)
        internal
    {
        accreditedInvestor[_investor] = false;
        emit WhiteListedAddressRemoved(_investor);
    }

    function addToWhiteList(address _investor)
        public
        onlyAdminOrOwner
    {
        _addToWhiteList(_investor);
    }

    function addManyToWhiteList(address[] _investors)
        public
        onlyAdminOrOwner
    {
        // gas limit – less then 2 400 000
        require(_investors.length <= 100);

        for (uint256 i = 0; i < _investors.length; i++) {
            _addToWhiteList(_investors[i]);
        }
    }

    function removeFromWhiteList(address _investor)
        public
        onlyAdminOrOwner
    {
        _removeFromWhiteList(_investor);
    }

    function removeManyFromWhiteList(address[] _investors)
        public
        onlyAdminOrOwner
    {
        // gas limit – less then 900 000
        require(_investors.length <= 100);

        for (uint256 i = 0; i < _investors.length; i++) {
            _removeFromWhiteList(_investors[i]);
        }
    }

    function addAdmin(address _addr)
        public
        onlyOwner
    {
        admins[_addr] = true;
    }

    function removeAdmin(address _addr)
        public
        onlyOwner
    {
        admins[_addr] = false;
    }

    function isInWhiteList(address _addr)
        public
        view
        returns (bool)
    {
        return accreditedInvestor[_addr];
    }

}