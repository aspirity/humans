/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;


interface PreSaleInterface {

    // Timestamp of end
    function endTime() external view returns (uint256);

    // Total money raised (number of tokens without bonuses)
    function totalRaised() external view returns (uint256);

    // Total tokens sold in this contract
    function totalTokensSold() external view returns (uint256);

}