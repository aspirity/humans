pragma solidity ^0.4.24;

/**
 * @title HumansToken interface
 * @dev IERC20 functions + mint function 
 */
interface HumansTokenInterface {

    function totalSupply() external view returns (uint256);

    function balanceOf(address who) external view returns (uint256);

    function allowance(address owner, address spender) external view returns (uint256);

    function transfer(address to, uint256 value) external returns (bool);

    function approve(address spender, uint256 value) external returns (bool);

    function transferFrom(address from, address to, uint256 value) external returns (bool);

    function mint(address _to, uint256 _amount) external returns (bool);

}