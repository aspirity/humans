/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./ownership/Ownable.sol";

/**
 * @title HumansRegistry
 * @dev Registry list for Accredited Addresses
 */
contract HumansRegistry is Ownable {

    event AccreditedAddressAdded(address addr);

    mapping(address => bool) public accreditedAddresses;


    // ** EXTERNAL FUNCTIONS **

    function addAddress(address _addr)
        external
        onlyOwner
    {
        _addAddress(_addr);
    }


    // ** PUBLIC VIEW FUNCTIONS **

    /**
     * @return true if _addr is the accredited address
     */
    function isAccredited(address _addr) public view returns (bool) {
        return accreditedAddresses[_addr];
    }


    // ** PRIVATE HELPER FUNCTIONS **

    function _addAddress(address _addr)
        internal
    {
        accreditedAddresses[_addr] = true;
        emit AccreditedAddressAdded(_addr);
    }

}