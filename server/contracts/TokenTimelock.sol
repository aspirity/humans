/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./token/ERC20/SafeERC20.sol";


/**
 * @title TokenTimelock
 * @dev TokenTimelock is a token holder contract that will allow a
 * beneficiaries to extract the tokens after a given release time
 */
contract TokenTimelock {
    using SafeERC20 for IERC20;

    // ERC20 basic token contract being held
    IERC20 public token;

    // beneficiaries of tokens after they are released
    address public advisoryAddress;
    address public managementAddress;

    // timestamp when token release is enabled
    uint256 public releaseTime;

    constructor(
        IERC20 _token,
        address _advisoryAddress,
        address _managementAddress,
        uint256 _releaseTime
    )
        public
    {
        // solium-disable-next-line security/no-block-members
        require(_releaseTime > block.timestamp);
        token = _token;
        advisoryAddress = _advisoryAddress;
        managementAddress = _managementAddress;
        releaseTime = _releaseTime;
    }

    /**
    * @notice Transfers tokens held by timelock to beneficiary.
    */
    function release() public {
        // solium-disable-next-line security/no-block-members
        require(block.timestamp >= releaseTime);

        uint256 amount = token.balanceOf(address(this));
        require(amount > 0);

        uint256 amountAdvisory = amount / 2;

        token.safeTransfer(advisoryAddress, amountAdvisory);
        token.safeTransfer(managementAddress, amount - amountAdvisory);
    }

}