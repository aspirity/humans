/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;


/**
 * @title CrowdSale interface
 * @dev StartTime and EndTime functions
 */
interface CrowdSaleInterface {

    function isActivated() external view returns (bool);

    function endTime() external view returns (uint256);

}