/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./HumansTokenInterface.sol";
import "./math/SafeMath.sol";
import "./WhiteList.sol";
import "./PriceTicker.sol";
import "./CrowdSaleInterface.sol";


contract PreSaleSAFT is PriceTicker {
    using SafeMath for uint256;


    // ** PUBLIC STATE VARIABLES **

    // Token being sold
    HumansTokenInterface public humansToken;

    // WhiteList for investors
    WhiteList public whiteList;

    // CrowdSale address
    CrowdSaleInterface public crowdSale;

    // Active States
    bool public isActivated;
    bool public isFinished;

    // Address where funds are transferred
    address public wallet;

    uint256 public minimumInvest; // in tokens

    uint256 public softCap; // in tokens

    // SAFT bonus
    uint256 public bonus;

    // Token balances of investors
    mapping(address => uint256) public tokenBalances;


    // ** PRIVATE STATE VARIABLES **

    // wei which has stored on SAFT contract
    mapping(address => uint256) private balancesForRefund;  // in wei (not public: only for refund)

    // is withdrawn paid tokens
    mapping(address => bool) private isWithdrawn;

    // information about investment will be updated after the response of Oraclize
    mapping(bytes32 => InvestInfo) private currentInvests;

    // Total money raised (number of tokens without bonuses)
    uint256 private _totalRaised = 1;  // equals 1 - gas optimization

    // ** EVENTS **

    /**
     * Event for tokens purchase logging
     * @param beneficiary who got the tokens
     * @param numberOfTokens purchased
     * @param weiReceived from investor
     */
    event TokenPurchase(
        address indexed beneficiary, 
        uint256 numberOfTokens,
        uint256 weiReceived
    );

    /**
     * Event for tokens withdrawal logging
     * @param beneficiary who got the tokens
     * @param numberOfTokens withdrawn
     */
    event TokenWithdrawal(
        address indexed beneficiary, 
        uint256 numberOfTokens
    );

    /**
     * Event for WEI refund logging
     * @param beneficiary who refund the tokens
     * @param weiRefunded from SAFT
     */
    event WeiRefund(
        address indexed beneficiary, 
        uint256 weiRefunded
    );

    // Event for set crowdsale address logging
    event SetCrowdSale(address crowdSale);

    // Event for state of the SAFT logging
    event SaleStateChanged(string state);


    // ** STRUCTS **

    struct InvestInfo {
        // @dev address of beneficiary
        address beneficiary;

        // @dev wei received from investor
        uint96 weiReceived;
    }


    // ** CONSTRUCTOR **

    /**
    * @dev Constructor of SAFT
    *
    * @param _wallet for withdrawal ether
    * @param _humansToken Humans Token
    * @param _whiteList address of WhiteList
    */
    constructor(
        address _wallet,
        address _humansToken,
        address _whiteList
    ) 
        public
        payable
    {
        require(_wallet != address(0));
        require(_whiteList != address(0));

        wallet = _wallet;
        humansToken = HumansTokenInterface(_humansToken);
        whiteList = WhiteList(_whiteList);

        // states in tokens
        minimumInvest = 50 * 1e18;  // equals 50$
        softCap = 5e6 * 1e18;  // equals 5 000 000$

        bonus = 50; // percent

        // update oraclize query gas limit for SAFT contract
        oraclizeGasLimit = 95000;
    }


    // ** MODIFIERS **

    // Allowed refund in case of unsuccess SAFT end
    modifier refundAllowed() {
        require(isFinished && _totalRaised <= softCap, "Refund is not active");
        _;
    }

    // Active period for tokens withdrawal
    modifier tokenWithdrawalActive() {
        require(isFinished && crowdSale.isActivated() && _totalRaised > softCap, "Token withdrawal is not active");
        _;
    }

    // Active period for eth withdrawal by owner
    modifier ethWithdrawalActive() {
        require(isFinished && _totalRaised > softCap, "ETH withdrawal is not active");
        _;
    }

    // Can this investor buy tokens now
    modifier canInvest(
        address _investor,
        uint256 _value)
    {
        require(isActivated && !isFinished);
        require(_value >= minimumInvest);
        require(whiteList.isInWhiteList(_investor));
        _;
    }


    // ** EXTERNAL FUNCTIONS **

    function() external payable {
        buyTokens(msg.sender);
    }

    // Withdrawal tokens from this contract after success SAFT by investor
    function withdrawTokens()
        external
    {
        _withdrawTokens(msg.sender);
    }

    // Refund ETH to the investors in case of under Soft Cap end
    function refund() 
        external 
        refundAllowed 
    {
        uint256 valueToReturn = balancesForRefund[msg.sender];

        // update state
        balancesForRefund[msg.sender] = 0;

        msg.sender.transfer(valueToReturn);

        emit WeiRefund(msg.sender, valueToReturn);
    }


    // ** ONLY OWNER FUNCTIONS **

    // Activate SAFT Sale
    function activateSale() 
        external 
        onlyOwner 
    {
        require(!isActivated, "SAFT has activated yet");
        isActivated = true;
        emit SaleStateChanged("Sale is activated");
    }

    // Finish SAFT Sale
    function finishSale() 
        external 
        onlyOwner 
    {
        require(isActivated && !isFinished, "Not valid state for finish");
        isFinished = true;
        emit SaleStateChanged("Sale is finished");
    }

    // Manual buy tokens by owner (e.g.: selling for fiat money)
    // @param _to address of beneficiary
    // @param _value of tokens without bonus tokens (1 token = 10^18)
    function manualBuy(
        address _to, 
        uint256 _value
    ) 
        external 
        onlyOwner
        canInvest(_to, _value)
    {
        // 3rd param - 0 wei received from investor (manual buy)
        _buyTokens(_to, _value, 0);
    }

    // Set the address of CrowdSale
    function setCrowdSale(address _crowdSale) 
        external 
        onlyOwner 
    {
        crowdSale = CrowdSaleInterface(_crowdSale);
        require(!crowdSale.isActivated(), "The CrowdSale being added is not valid");
        emit SetCrowdSale(crowdSale);
    }

    // Set the address of Humans Token
    function setToken(address _tokenAddress) 
        external 
        onlyOwner 
    {
        humansToken = HumansTokenInterface(_tokenAddress);
        require(humansToken.balanceOf(this) >= 0, "The token being added is not ERC20 token");
    }

    // Withdrawal eth from contract
    function withdrawalEth() 
        external
        onlyOwner
        ethWithdrawalActive
    {
        // withdrawal all eth from contract
        _forwardFunds(address(this).balance);
    }

    /**
    * @dev Transfer tokens to beneficiary by owner
    *
    * @param _beneficiary SAFT investor
    */
    function transferTokens(
        address _beneficiary
    ) 
        external 
        onlyOwner 
    {
        _withdrawTokens(_beneficiary);
    }

    /**
    * @dev Transfer tokens to array of beneficiaries by owner
    *
    * @param _beneficiaries array of SAFT investors
    */
    function transferTokens(
        address[] _beneficiaries
    ) 
        external 
        onlyOwner 
    {
        require(_beneficiaries.length > 0, "array lengths have to be greater than zero");

        for (uint256 i = 0; i < _beneficiaries.length; i++) {
            _withdrawTokens(_beneficiaries[i]);
        }
    }


    // ** PUBLIC VIEW FUNCTIONS **

    /**
    * @dev Total money raised (number of tokens without bonuses)
    */
    function totalRaised() public view returns (uint256) {
        return _totalRaised.sub(1);
    }

    /**
    * @dev Total tokens sold in this contract
    */
    function totalTokensSold() public view returns (uint256) {
        return _getTokenNumberWithBonus(_totalRaised.sub(1));
    }


    // ** PUBLIC FUNCTIONS **

    // Low level token purchase function
    // @param _beneficiary address of beneficiary
    function buyTokens(address _beneficiary) 
        public
        payable
        canInvest(_beneficiary, msg.value.mul(rate).div(10 ** decimals))
    {
        // update balance for refund
        balancesForRefund[_beneficiary] = balancesForRefund[_beneficiary].add(msg.value);

        // update rate using Oraclize and then update investor's balance
        _updateRate(_beneficiary, msg.value);
    }


    // ** PRIVATE HELPER FUNCTIONS **

    // Get number of tokens with bonus
    // @param _value in tokens without bonus
    function _getTokenNumberWithBonus(uint256 _value) 
        internal 
        view 
        returns (uint256) 
    {
        return _value.add(_value.mul(bonus).div(100));
    }

    // Send weis to the wallet
    // @param _value in wei
    function _forwardFunds(uint256 _value) 
        internal 
    {
        wallet.transfer(_value);
    }

    /**
     * Buy tokens - helper function
     * @param _beneficiary address of beneficiary
     * @param _value of tokens (1 token = 10^18)
     * @param _weiReceived from investor
     */
    function _buyTokens(
        address _beneficiary,
        uint256 _value,
        uint256 _weiReceived
    )
        internal
    {
        uint256 tokensWithBonus = _getTokenNumberWithBonus(_value);

        // update states
        tokenBalances[_beneficiary] = tokenBalances[_beneficiary].add(tokensWithBonus);
        _totalRaised = _totalRaised.add(_value);

        emit TokenPurchase(_beneficiary, tokensWithBonus, _weiReceived);
    }

    // Helper-function: withdrawal tokens from this contract after success SAFT
    // Paid tokens - after the start of crowdSale
    // Bonus tokens - after the finish of crowdSale
    function _withdrawTokens(
        address _beneficiary
    )
        internal
        tokenWithdrawalActive
    {
        uint256 value = tokenBalances[_beneficiary];

        if (crowdSale.endTime() > now) {
            // withdraw all tokens
            isWithdrawn[_beneficiary] = false; // gas optimization
        } else if (!isWithdrawn[_beneficiary]) {
            // withdraw tokens without bonus
            isWithdrawn[_beneficiary] = true;
            value = value.mul(100).div(100 + bonus);
        } else {
            value = 0;
        }

        require(value > 0);

        // update state
        tokenBalances[_beneficiary] = tokenBalances[_beneficiary].sub(value);

        require(humansToken.mint(_beneficiary, value), "tokens are not minted");
        emit TokenWithdrawal(_beneficiary, value);
    }


    // ** PriceTicker Contract FUNCTIONS **

    // Helper: oraclize query
    function _updateRate(address _beneficiary, uint256 _value) 
        internal
    {   
        if (oraclize_getPrice("URL", oraclizeGasLimit) > address(this).balance) {
            emit NewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            emit NewOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            bytes32 queryId = oraclize_query("URL", apiQuery, oraclizeGasLimit);

            // update investment info
            currentInvests[queryId].beneficiary = _beneficiary;
            currentInvests[queryId].weiReceived = uint96(_value);
        }
    }

    // OVERRIDING Helper: oraclize callback
    function _oraclizeCallback(
        bytes32 _queryId,
        string _result,
        bytes _proof
    )
        internal
    {
        // change rate value
        super._oraclizeCallback(_queryId, _result, _proof);

        // update balance of beneficiary
        if (currentInvests[_queryId].beneficiary != address(0)) {
            _buyTokens(currentInvests[_queryId].beneficiary, (uint256)(currentInvests[_queryId].weiReceived).mul(rate).div(10 ** decimals), (uint256)(currentInvests[_queryId].weiReceived));
            delete currentInvests[_queryId];
        }
    }

}