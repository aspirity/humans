/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./HumansTokenInterface.sol";
import "./WhiteList.sol";
import "./PreSaleInterface.sol";
import "./PriceTicker.sol";
import "./math/SafeMath.sol";


contract CrowdSale is PriceTicker {
    using SafeMath for uint256;


    // ** PUBLIC STATE VARIABLES **

    // Token being sold
    HumansTokenInterface public humansToken;

    // WhiteList for investors
    WhiteList public whiteList;

    // SAFT
    PreSaleInterface public preSaleSAFT;

    // Activate State
    bool public isActivated;

    // Timestamps of end
    uint256 public endTime;

    // Address where funds are transferred
    address public wallet;

    // Admins' addresses where tokens are transferred after success finish
    address public holders;
    address public timelockAddress;
    address public reservedAddress;

    uint256 public minimumInvest; // in tokens

    uint256 public hardCap; // in tokens

    // Total money raised include SAFT (number of tokens without bonuses)
    uint256 public totalRaised;  // in tokens


    // ** EVENTS **

    /**
     * Event for tokens purchase logging
     * @param beneficiary who got the tokens
     * @param numberOfTokens purchased
     */
    event TokenPurchase(
        address indexed beneficiary, 
        uint256 numberOfTokens
    );

    // Event for set preSale address logging
    event SetPreSale(address preSale);

    // Event for state of the CrowdSale logging
    event SaleStateChanged(string state);


    // ** CONSTRUCTOR **

    /**
    * @dev Constructor of CrowdSale
    *
    * @param _wallet for withdrawal ether
    * @param _humansToken Humans Token
    * @param _whiteList address of WhiteList
    * @param _holders for token transfering
    * @param _timelockAddress for token vesting
    * @param _reservedAddress for reserved tokens
    * @param _preSaleSAFT address of SAFT
    */
    constructor(
        address _wallet,
        address _humansToken,
        address _whiteList,
        address _holders,
        address _timelockAddress,
        address _reservedAddress,
        address _preSaleSAFT
    ) 
        public
        payable
    {
        require(_wallet != address(0));
        require(_whiteList != address(0));
        require(_holders != address(0));
        require(_timelockAddress != address(0));
        require(_reservedAddress != address(0));

        wallet = _wallet;
        humansToken = HumansTokenInterface(_humansToken);
        whiteList = WhiteList(_whiteList);

        holders = _holders;
        timelockAddress = _timelockAddress;
        reservedAddress = _reservedAddress;

        // states in tokens
        minimumInvest = 50 * 1e18;  // equals 50$
        hardCap = 1e8 * 1e18;  // equals 100 000 000$

        _setPreSale(_preSaleSAFT);

        // update oraclize query gas limit for CrowdSale contract
        oraclizeGasLimit = 97500;
    }


    // ** MODIFIERS **

    // @return true if the transaction can buy tokens
    modifier saleIsOn() {
        require(isActivated && now <= endTime);
        _;
    }

    // @return true if PreSale under hard cap
    modifier isUnderHardCap(uint256 _value) {
        require(totalRaised.add(_value) <= hardCap);
        _;
    }

    // Active period for tokens withdrawal
    modifier isCrowdSaleFinished() {
        require(hasEnded());
        _;
    }


    // ** EXTERNAL FUNCTIONS **

    function() external payable {
        buyTokens(msg.sender);
    }


    // ** ONLY OWNER FUNCTIONS **

    // Activate CrowdSale
    function activateSale() 
        external 
        onlyOwner 
    {
        require(!isActivated, "CrowdSale has activated yet");

        // update states
        isActivated = true;
        endTime = now + 61 days;  // 2 months
        totalRaised = preSaleSAFT.totalRaised();

        emit SaleStateChanged("Sale is activated");

        _updateRate(0);
    }

    // Manual buy tokens by owner (e.g.: selling for fiat money)
    // @param _to address of beneficiary
    // @param _value of tokens (1 token = 10^18)
    function manualBuy(
        address _to, 
        uint256 _value
    ) 
        external 
        onlyOwner 
    {
        _buyTokens(_to, _value);
    }

    // Set the address of PreSale SAFT
    function setPreSale(address _preSale) 
        external 
        onlyOwner 
    {
        _setPreSale(_preSale);
    }

    // Set the address of Humans Token
    function setToken(address _tokenAddress) 
        external 
        onlyOwner 
    {
        humansToken = HumansTokenInterface(_tokenAddress);
        require(humansToken.balanceOf(this) >= 0, "The token being added is not ERC20 token");
    }

    // Withdrawal eth from contract
    function withdrawalEth() 
        external 
        onlyOwner
    {
        // withdrawal all eth from contract
        _forwardFunds(address(this).balance);
    }

    // Success finish of CrowdSale
    function finishCrowdSale() 
        external 
        onlyOwner
        isCrowdSaleFinished
    {
        // withdrawal all eth from contract
        _forwardFunds(address(this).balance);

        // number of sold tokens == number of reserved tokens
        uint256 totalSoldTokens = totalRaised.sub(preSaleSAFT.totalRaised())
            .add(preSaleSAFT.totalTokensSold());

        // mint Humans tokens to admins
        humansToken.mint(holders, totalSoldTokens.div(2));  // 50% to holders
        humansToken.mint(timelockAddress, totalSoldTokens.mul(2).div(5));  // 40% to timelock contract
        humansToken.mint(reservedAddress, totalSoldTokens.div(10));  // 10% reserved
    }


    // ** PUBLIC FUNCTIONS **

    // @return true if PreSale has ended
    function hasEnded() 
        public 
        view
        returns (bool) 
    {
        return (isActivated && (now > endTime || totalRaised >= hardCap));
    }

    // Low level token purchase function
    // @param _beneficiary address of beneficiary
    function buyTokens(address _beneficiary) 
        public
        payable 
    {
        uint256 value = msg.value.mul(rate).div(10 ** decimals);
        _buyTokens(_beneficiary, value);
    }


    // ** PRIVATE HELPER FUNCTIONS **

    // Helper: Set the address of PreSale SAFT
    function _setPreSale(address _preSale) 
        internal
    {
        preSaleSAFT = PreSaleInterface(_preSale);
        require(preSaleSAFT.totalRaised() >= 0, "The SAFT being added is not valid");
        emit SetPreSale(preSaleSAFT);
    }

    // Send weis to the wallet
    // @param _value in wei
    function _forwardFunds(uint256 _value) 
        internal 
    {
        wallet.transfer(_value);
    }

    /**
     * Buy tokens - helper function
     * @param _beneficiary address of beneficiary
     * @param _value of tokens (1 token = 10^18)
     */
    function _buyTokens(
        address _beneficiary,
        uint256 _value
    ) 
        internal 
        isUnderHardCap(_value) 
        saleIsOn
    {
        require(_beneficiary != address(0));
        require(_value >= minimumInvest);
        require(whiteList.isInWhiteList(_beneficiary));

        // update state
        totalRaised = totalRaised.add(_value);

        humansToken.mint(_beneficiary, _value);
        emit TokenPurchase(_beneficiary, _value);
    }


    // ** PriceTicker Contract FUNCTIONS **

    // OVERRIDING Helper: oraclize callback
    function _oraclizeCallback(
        bytes32 _queryId,
        string _result,
        bytes _proof
    )
        internal
    {
        // change rate value
        super._oraclizeCallback(_queryId, _result, _proof);

        // update price after 10 minute delay
        if (isActivated && now <= endTime) {
            _updateRate(10 minutes);
        }
    }

}