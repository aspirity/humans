/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./ownership/Ownable.sol";
import "./oraclize/oraclizeAPI_0.4.25.sol";


/**
 * @title PriceTicker
 * @dev Smart contract for check ETH price from coinmarketcap
 */
contract PriceTicker is Ownable, usingOraclize {


    // ** EVENTS **

    // Event for new oraclize query logging
    event NewOraclizeQuery(string description);

    // Event for new ETH price ticker logging
    event NewPriceTicker(bytes32 myid, string price, bytes proof);

    // Event for new gas limit logging
    event SetNewGasLimit(uint256 newGasLimit);

    // Event for new gas price logging
    event SetNewGasPrice(uint256 newGasPrice);

    // Event for set oraclize proof logging
    event SetProof(string proofType);

    // Event for new API query logging
    event SetNewApiQuery(string newQuery);


    // ** PUBLIC STATE VARIABLES **

    // How many token units a buyer gets per 1 wei
    uint256 public rate;

    // Oraclize query gas limit
    uint256 public oraclizeGasLimit;

    // Oraclize query gas price
    uint256 public oraclizeGasPrice;

    // API query
    string public apiQuery;

    // Decimals for ETH price
    uint256 public decimals;


    // ** CONSTRUCTOR **

    /**
    * @dev Constructor of PriceTicker Contract
    */
    constructor() 
        public 
        payable
    {
        // set oraclize query gas limit
        oraclizeGasLimit = 50000;

        // set oraclize gas price
        oraclizeGasPrice = 10000000000 wei;  // 10 gwei
        oraclize_setCustomGasPrice(oraclizeGasPrice);

        // set decimals for ETH price
        decimals = 10;

        // set default rate before Oraclize update
        rate = 500;  // gas optimization

        // set API query
        _setApiQuery("json(https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=USD).0.price_usd");

        // set oraclize proof - NONE
        oraclize_setProof(proofType_NONE);
    }

    
    // ** EXTERNAL PAYABLE FUNCTIONS **

    function() external payable {}


    // ** ONLY OWNER FUNCTIONS **

    /**
    * @dev Set new API query
    */
    function setApiQuery(string queryString) 
        external
        onlyOwner
    {
        _setApiQuery(queryString);
    }

    /**
    * @dev Set new decimals for ETH price
    */
    function setDecimals(uint256 newDecimals) 
        external
        onlyOwner
    {
        require(newDecimals <= 10, "Decimals cannot be more than 10");
        decimals = newDecimals;
    }

    /**
    * @dev Set new gas limit for oraclize query
    */
    function setGasLimit(uint256 gasLimit) 
        external
        onlyOwner
    {
        require(gasLimit > 0, "gasLimit cannot be zero");
        oraclizeGasLimit = gasLimit;
        emit SetNewGasLimit(gasLimit);
    }

    /**
    * @dev Set new gas price for oraclize query
    */
    function setGasPrice(uint256 gasPrice) 
        external
        onlyOwner
    {
        require(gasPrice > 0, "gasPrice cannot be zero");
        oraclizeGasPrice = gasPrice;
        oraclize_setCustomGasPrice(oraclizeGasPrice);
        emit SetNewGasPrice(oraclizeGasPrice);
    }

    /**
    * @dev Enable oraclize proof
    * Increase of query cost
    */
    function enableProof()
        external
        onlyOwner
    {
        oraclize_setProof(proofType_TLSNotary | proofStorage_IPFS);
        emit SetProof("TLSNotary and IPFS");
    }

    /**
    * @dev Disable oraclize proof
    * Decrease of query cost
    */
    function disableProof()
        external
        onlyOwner
    {
        oraclize_setProof(proofType_NONE);
        emit SetProof("NONE");
    }

    /**
    * @dev Manual call of update rate 
    */
    function updateRate()
        external
        onlyOwner
    {
        _updateRate(0);
    }


    // ** ORACLIZE CALLBACKS **

    function __callback(
        bytes32 myId,
        string result
    )
        public 
    {
        require(msg.sender == oraclize_cbAddress(), "Sender is not Oraclize address");
        _oraclizeCallback(myId, result, "NONE");
    }

    function __callback(
        bytes32 myId,
        string result,
        bytes proof
    )
        public 
    {
        require(msg.sender == oraclize_cbAddress(), "Sender is not Oraclize address");
        _oraclizeCallback(myId, result, proof);
    }


    // ** PRIVATE HELPER FUNCTIONS **

    // Helper: set API query
    function _setApiQuery(string queryString) 
        internal
    {
        apiQuery = queryString;
        _updateRate(0);
        emit SetNewApiQuery(queryString);
    }

    // Helper: oraclize query
    // updateTime - update delay
    function _updateRate(uint256 updateTime) 
        internal
    {   
        if (oraclize_getPrice("URL", oraclizeGasLimit) > address(this).balance) {
            emit NewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            emit NewOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query(updateTime, "URL", apiQuery, oraclizeGasLimit);
        }
    }

    //Helper: oraclize callback
    function _oraclizeCallback(
        bytes32 queryId,
        string result,
        bytes proof
    )
        internal
    {
        rate = parseInt(result, decimals); // price in USD * 10^decimals
        emit NewPriceTicker(queryId, result, proof);
    }
}