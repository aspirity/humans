/**
 * @author https://github.com/Dmitx
 */

pragma solidity ^0.4.24;

import "./token/ERC20/ERC20Capped.sol";
import "./token/ERC20/ERC20Pausable.sol";
import "./ownership/Ownable.sol";
import "./HumansRegistry.sol";


contract HumansToken is ERC20Capped, ERC20Pausable, Ownable {

    // ** PUBLIC STATE VARIABLES **

    string public constant name = "Humans Gen";

    string public constant symbol = "HG";

    uint8 public constant decimals = 18;

    HumansRegistry public registryAddress;


    // ** EVENTS **

    /**
     * Event for setting registry address logging
     * @param registryAddr The address of HumansRegistry
     */
    event RegistryAddressSet(address indexed registryAddr);


    // ** CONSTRUCTOR **    

    // set Max Total Supply in 10 000 000 000 tokens and Registry address
    constructor(address _registryAddr) 
        public
        ERC20Capped(1e10 * 1e18) 
    {
        _setRegistryAddress(_registryAddr);
    }


    // ** ERC20 OVERWRITE FUNCTIONS **

    function transfer(address to, uint256 value) public returns (bool) {
        // 3e22 == 30 000 tokens
        if (value <= 3e22 || registryAddress.isAccredited(to)){
            return super.transfer(to, value);
        }
        return false;
    }

    function transferFrom(address from, address to, uint256 value) public returns (bool) {
        // 3e22 == 30 000 tokens
        if (value <= 3e22 || registryAddress.isAccredited(to)){
            return super.transferFrom(from, to, value);
        }
        return false;
    }


    // ** ONLY OWNER FUNCTIONS **

    // Change the address of Registry
    function setRegistryAddress(address _registryAddr)
        external 
        onlyOwner
    {
        _setRegistryAddress(_registryAddr);
    }

    
    // ** PRIVATE HELPER FUNCTIONS **

    // Helper: Set the address of Registry
    function _setRegistryAddress(address _registryAddr) 
        internal 
    {
        registryAddress = HumansRegistry(_registryAddr);

        // check valid address of HumansRegistry
        registryAddress.isAccredited(address(this));

        emit RegistryAddressSet(_registryAddr);
    }

}